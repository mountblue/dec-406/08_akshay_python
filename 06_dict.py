#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 17:26:52 2018

@author: akshay
"""

num = int(input())
myDict = {i:i*i for i in range(1,num+1)}
print(myDict)
