#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 17:04:42 2018

@author: akshay
"""
trans_log = []
net_amount = 0
while(True):
    line = input()
    if(line):
        trans_log.append(line)
    else:
        break
for i in trans_log:
    if i[0] == 'D' or i[0] == 'd':
        net_amount += int(i[2:])
    elif i[0] == 'W' or i[0] == 'w':
        net_amount -= int(i[2:])
print(net_amount)