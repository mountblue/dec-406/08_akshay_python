#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 16:59:12 2018

@author: akshay
"""
num = input().split(',')
num = [int(i)**2 for i in num if int(i) % 2 != 0]
print(",".join(map(str,num)))
