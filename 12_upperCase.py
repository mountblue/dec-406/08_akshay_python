#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 15:13:49 2018

@author: akshay
"""
lines = []
while True:
    line = input()
    if line:
        lines.append(line.upper())
    else:
        break
print('\n'.join(lines))