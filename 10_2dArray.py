#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 14:47:44 2018

@author: akshay
"""

dimension = input().split(',')
x = int(dimension[0])
y = int(dimension[1])
arr = []
for i in range(x):
    new = []
    for j in range(y):
        new.append(i*j)
    arr.append(new)
print(arr)