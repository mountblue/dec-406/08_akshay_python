#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 15:05:59 2018

@author: akshay
"""
words = input().split(',')
words.sort()
print(",".join(words))