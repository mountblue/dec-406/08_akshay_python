#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 16:19:15 2018

@author: akshay
"""
string = input()
digits = 0
letters = 0
for word in string:
    if word.isdigit():
        digits += 1
    elif word.isalpha():
        letters += 1
print("LETTERS {0} \n DIGITS {1}".format(letters,digits))