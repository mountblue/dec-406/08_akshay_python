#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 15:47:18 2018

@author: akshay
"""
for i in range(1000,3001):
    number = str(i)
    temp = [j for j in number if int(j)%2==0]
    if len(number)!=len(temp):
        continue;
    else:
        print(number,end=',')
