#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 17:42:47 2018

@author: akshay
"""
from math import sqrt
c = 50
h = 30
numbers = input().split(',')
for i in numbers:
    print(int(sqrt((2*c*int(i)/h))),end=",")