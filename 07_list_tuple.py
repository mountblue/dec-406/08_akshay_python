#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 17:31:18 2018

@author: akshay
"""
numbers = input().split(',')
myList = [int(i) for i in numbers]
myTuple = tuple(myList)
print(myList,myTuple, sep='\n')