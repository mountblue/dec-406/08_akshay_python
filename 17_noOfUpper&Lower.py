#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 16:25:51 2018

@author: akshay
"""

string = input()
upper = 0
lower = 0
for word in string:
    if word.isupper():
        upper += 1
    elif word.islower():
        lower += 1
print("UPPER CASE {0} \n LOWER CASE {1}".format(upper,lower))