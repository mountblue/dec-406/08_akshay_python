#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 15:33:14 2018

@author: akshay
"""
numbers = input().split(',')
for num in numbers:
    num = int(num,2)
    if num % 5 == 0:
        print(bin(num)[2:],end=',')
