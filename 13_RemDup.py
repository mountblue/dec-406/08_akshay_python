#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 15:23:18 2018

@author: akshay
"""
words = input().split(' ')
print(' '.join(map(str,sorted(list(set(words))))))
