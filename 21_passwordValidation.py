#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 17:13:23 2018

@author: akshay
"""
import re
string = input().split(',')
result = []
for s in string:
    if (len(s) < 6 or len(s) > 12 or re.search('[a-z]',s) == None or re.search('[A-Z]',s) == None or re.search('[0-9]',s) == None or re.search('[$#@]',s) == None):
        continue;
    else:
        result.append(s)
print(",".join(result))

