#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 17:35:02 2018

@author: akshay
"""
class myClass:
    def getString(self):
        self.string = input("Enter String: ")
    def printString(self):
        print(self.string)

obj = myClass()
obj.getString()
obj.printString()