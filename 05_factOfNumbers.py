#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 24 17:18:25 2018

@author: akshay
"""
def fact(num):
    if num < 1:
        return 1
    else:
        return num * fact(num-1)
numbers = input('Enter : ').split(' ')
numbers = [int(i) for i in numbers]
result = []
for i in numbers:
    result.append(fact(i))
print(','.join((map(str,result))))
